package de.niklasweiblen.uebung2.repository;

import de.niklasweiblen.uebung2.entity.Movie;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
class MovieRepositoryTest {

    @Autowired
    private MovieRepository movieRepository;

    @AfterEach
    public void tearDown() {
        movieRepository.deleteAll();
    }

    @Test
    void findMoviesWithExistingName() {
        String movieName = "Harry Potter und die Kammer des Schreckens";
        Movie movie = new Movie(movieName);
        movieRepository.save(movie);

        List<Movie> moviesByName = movieRepository.findMovieByName(movieName);

        assertEquals(movieName, moviesByName.get(0).getName());
    }

    @Test
    void findMoviesWithNonExistingName() {
        String movieName = "Harry Potter und die Kammer des Schreckens";
        Movie movie = new Movie(movieName);
        movieRepository.save(movie);
        String wrongMovieName = "Harry Potter und die geheime Spring-Boot Kammer";
        List<Movie> moviesByName = movieRepository.findMovieByName(wrongMovieName);

        assertTrue(moviesByName.isEmpty());

    }
}