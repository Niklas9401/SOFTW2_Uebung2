package de.niklasweiblen.uebung2.service;

import de.niklasweiblen.uebung2.entity.Movie;
import de.niklasweiblen.uebung2.repository.MovieRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MovieServiceImplementationTest {

    private MovieService movieService;

    @Mock
    private MovieRepository movieRepository;

    @BeforeEach
    void setUp() {
        this.movieService = new MovieServiceImplementation(movieRepository);
    }

    @Test
    void addMovie() {
        String movieName = "Kingsman: The Secret Service";
        Movie movie = new Movie(movieName);
        movieService.addMovie(movie);

        verify(movieRepository).save(movie);
    }

    @Test
    void getMovieWithExistingId() {
        String movieName = "Kingsman: The Secret Service";
        when(movieRepository.findMovieById(0L)).thenReturn(Optional.of(new Movie(movieName)));
        Optional<Movie> movieById = movieService.getMovieWithId(0L);

        verify(movieRepository).findMovieById(0L);
        assertTrue(movieById.isPresent());
        Assertions.assertThat(movieName).isEqualTo(movieById.get().getName());
    }

    @Test
    void getMovieWithNotExistingId() {
        when(movieRepository.findMovieById(1L)).thenReturn(Optional.empty());
        Optional<Movie> movieById = movieService.getMovieWithId(1L);
        verify(movieRepository).findMovieById(1L);
        assert(movieById.isEmpty());
    }

    @Test
    void getMovieWithName() {
        String movieName = "Kingsman: The Secret Service";
        movieService.getMovieWithName(movieName);
        verify(movieRepository).findMovieByName(movieName);

    }

    @Test
    void getAllMovies() {
        movieService.getAllMovies();
        verify(movieRepository).findAll();

    }
}