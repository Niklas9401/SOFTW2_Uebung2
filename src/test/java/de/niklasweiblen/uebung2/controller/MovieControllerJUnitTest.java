package de.niklasweiblen.uebung2.controller;

import de.niklasweiblen.uebung2.controller.MovieController;
import de.niklasweiblen.uebung2.entity.Movie;
import de.niklasweiblen.uebung2.service.MovieService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class MovieControllerJUnitTest {

    MovieController movieController;

    @Mock
    MovieService movieService;

    @BeforeEach
    void setUp() {
        this.movieController = new MovieController(movieService);
    }

    @Test
    void testGetMovieMethod() {
        when(movieService.getAllMovies()).thenReturn(Collections.emptyList());
        ResponseEntity<List<Movie>> allMovies = movieController.getAllMovies();
        verify(movieService).getAllMovies();

        List<Movie> listOfMovies = allMovies.getBody();
        assertThat(allMovies.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(listOfMovies).isEqualTo(Collections.emptyList());
    }

    @Test
    void testAddMovieMethodWithEmptyMovieName() {
        Movie movie = new Movie("");
        ResponseEntity<Movie> movieResponseEntity = movieController.addMovie(movie);
        verify(movieService, never()).addMovie(movie);

        assertThat(movieResponseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);

    }

    @Test
    void testAddMovieMethodWithoutMovieName() {
        Movie movie = new Movie();
        ResponseEntity<Movie> movieResponseEntity = movieController.addMovie(movie);
        verify(movieService, never()).addMovie(movie);

        assertThat(movieResponseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    void testAddMovieMethodWithMovieName() {
        Movie movie = new Movie("Kingsman: The Secret Service");
        ResponseEntity<Movie> movieResponseEntity = movieController.addMovie(movie);
        verify(movieService).addMovie(movie);
        assertThat(movieResponseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
    }

    @Test
    void checkForAddedMovie() {
        String movieName = "Harry Potter und der Orden des Phönix";
        when(movieService.getAllMovies()).thenReturn(List.of(new Movie(movieName)));
        Movie movie = new Movie(movieName);
        ResponseEntity<List<Movie>> getAllMovies = movieController.getAllMovies();
        verify(movieService).getAllMovies();
        List<Movie> existingMovies = getAllMovies.getBody();
        assertThat(existingMovies).contains(movie);

    }

    @Test
    void checkForNonExistingMovieWithIdParam() {
        ResponseEntity<Movie> movieById = movieController.getMovieById(0L);
        verify(movieService).getMovieWithId(0L);
        assertThat(movieById.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    void checkForExistingMovieWithIdParam() {
        String movieName = "Harry Potter und der Stein der Weisen";
        when(movieService.getMovieWithId(0L)).thenReturn(Optional.of(new Movie(movieName)));
        ResponseEntity<Movie> movieById = movieController.getMovieById(0L);
        verify(movieService).getMovieWithId(0L);

        Movie movie = movieById.getBody();
        assert movie != null;
        assertThat(movie.getName()).isEqualTo(movieName);
    }

    @Test
    void checkForNonExistingMovieWithNameParam() {
        String movieName = "Not existing Movie";
        ResponseEntity<List<Movie>> movieByName = movieController.getMoviesByName(movieName);
        verify(movieService).getMovieWithName(movieName);
        assertThat(movieByName.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(movieByName.getBody()).isEqualTo(Collections.emptyList());
    }

    @Test
    void checkForExistingMovieWithNameParam() {
        //given
        String movieName = "Harry Potter und der Orden des Phönix";
        Movie movie = new Movie(movieName);
        when(movieService.getMovieWithName(movieName)).thenReturn(List.of(movie));

        //when
        ResponseEntity<List<Movie>> moviesByName = movieController.getMoviesByName(movie.getName());

        //then
        verify(movieService).getMovieWithName(movie.getName());
        assertThat(moviesByName.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(moviesByName.getBody()).isNotNull();
        assertThat(moviesByName.getBody()).isEqualTo(List.of(movie));

    }
}
