package de.niklasweiblen.uebung2.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.niklasweiblen.uebung2.controller.MovieController;
import de.niklasweiblen.uebung2.entity.Movie;
import de.niklasweiblen.uebung2.repository.MovieRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class MovieControllerIntegrationTest {

    @Autowired
    MovieRepository movieRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    MockMvc mockMvc;


    @Test
    void checkMovieCreationThroughAllLayers() throws Exception {
        String movieName = "Kingsman: The Secret Service";
        Movie movie = new Movie(movieName);

        mockMvc.perform(post("/movies")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(movie)))
                .andExpect(status().isCreated());

        Movie expectedMovie = new Movie(0L, movieName);
        Optional<Movie> movieEntity = movieRepository.findMovieById(expectedMovie.getId());
        assertTrue(movieEntity.isPresent());
        Assertions.assertThat(movieEntity.get()).isEqualTo(expectedMovie);
    }

    //Testfall 1
    @Test
    void successfulRequestWithEmptyListAsResponse() throws Exception {
        mockMvc.perform(get("/movies")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json("[]"));


    }

    //Testfall 2
    @Test
    void creatingExampleMovie() throws Exception {
        String movieName = "Top-Gun";
        Movie movie = new Movie(movieName);


        mockMvc.perform(MockMvcRequestBuilders
                        .post("/movies")
                        .content(objectMapper.writeValueAsString(movie))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$").doesNotExist());

        Movie expectedMovie = new Movie(0L, movieName);
        List<Movie> movieEntity = movieRepository.findMovieByName(expectedMovie.getName());
        Assertions.assertThat(movieEntity).isEqualTo(List.of(expectedMovie));
    }


    //Testfall 3
    @Test
    void addMovieAndCheckExistence() throws Exception {
        String movieName = "Who am I";
        Movie movie = new Movie(movieName);

        mockMvc.perform(MockMvcRequestBuilders
                .post("/movies")
                .content(objectMapper.writeValueAsString(movie))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON));


        mockMvc.perform(get("/movies")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[0].id").value(0))
                .andExpect(jsonPath("$.[0].name").value(movieName))
                .andExpect(jsonPath("$.*", hasSize(1)));

        Movie expectedMovie = new Movie(0L, movieName);
        List<Movie> movieEntity = movieRepository.findMovieByName(expectedMovie.getName());
        Assertions.assertThat(movieEntity).isEqualTo(List.of(expectedMovie));
    }

    //Testfall 4
    @Test
    void addTwoMoviesandCheckExistence() throws Exception {
        String movieName1 = "King Kong";
        String movieName2 = "300";

        Movie movie1 = new Movie(movieName1);
        Movie movie2 = new Movie(movieName2);

        mockMvc.perform(MockMvcRequestBuilders
                .post("/movies")
                .content(objectMapper.writeValueAsString(movie1))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON));

        mockMvc.perform(MockMvcRequestBuilders
                .post("/movies")
                .content(objectMapper.writeValueAsString(movie2))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON));

        mockMvc.perform(get("/movies")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[0].id").value(0))
                .andExpect(jsonPath("$.[0].name").value(movieName1))
                .andExpect(jsonPath("$.[1].id").value(1))
                .andExpect(jsonPath("$.[1].name").value(movieName2));

        Movie expectedMovie1 = new Movie(0L, movieName1);
        Movie expectedMovie2 = new Movie(1L, movieName2);

        List<Movie> movieEntity = movieRepository.findMovieByName(expectedMovie1.getName());
        Assertions.assertThat(movieEntity).isEqualTo(List.of(expectedMovie1));
        movieEntity = movieRepository.findMovieByName(expectedMovie2.getName());
        Assertions.assertThat(movieEntity).isEqualTo(List.of(expectedMovie2));
    }

    //Testfall 5
    @Test
    void createMovieWithWrongJSONKeyNameAndCheckForExistence() throws Exception {
        //Falsche Eingabe intentioniert
        String movie = "{\"namos\": \"Die Unglaublichen\"}";


        mockMvc.perform(MockMvcRequestBuilders
                        .post("/movies")
                        .content(movie)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$").doesNotExist());

    }

    //Testfall 6
    @Test
    void createTwoMoviesWithOneInvalidInputAndCheckForAllMovies() throws Exception {
        //Falsche Eingabe intentioniert
        String movie = "{\"namos\": \"Die Unglaublichen\"}";

        mockMvc.perform(MockMvcRequestBuilders
                .post("/movies")
                .content(movie)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON));

        String movieName = "San Andreas";
        Movie movie1 = new Movie(movieName);

        mockMvc.perform(MockMvcRequestBuilders
                .post("/movies")
                .content(objectMapper.writeValueAsString(movie1))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON));

        mockMvc.perform(get("/movies")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[0].id").value(0))
                .andExpect(jsonPath("$.[0].name").value(movieName))
                .andExpect(jsonPath("$.*", hasSize(1)));

        Movie expectedMovie = new Movie(0L, movieName);

        List<Movie> allMovies = movieRepository.findAll();
        Assertions.assertThat(allMovies.size()).isEqualTo(1);
        List<Movie> movieEntity = movieRepository.findMovieByName(expectedMovie.getName());
        Assertions.assertThat(movieEntity).isEqualTo(List.of(expectedMovie));
    }

    //########################################################################################
    //########################################  Teil 2  ######################################
    //########################################################################################

    // AUFGABE 1

    //Testfall 1
    @Test
    void checkForEmptyMovieListWithIdParam() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/movies/1"))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$").doesNotExist());
    }

    //Testfall 2
    @Test
    void checkForAddedMovieWithIdParam() throws Exception {

        String movieName = "GhostBusters";
        Movie movie = new Movie(movieName);

        mockMvc.perform(MockMvcRequestBuilders
                .post("/movies")
                .content(objectMapper.writeValueAsString(movie))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON));

        mockMvc.perform(MockMvcRequestBuilders
                        .get("/movies/0")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(0))
                .andExpect(jsonPath("$.name").value(movieName));


        Movie expectedMovie = new Movie(0L, movieName);
        List<Movie> movieEntity = movieRepository.findMovieByName(expectedMovie.getName());
        Assertions.assertThat(movieEntity).isEqualTo(List.of(expectedMovie));
    }

    // AUFGABE 2

    //Testfall 1
    @Test
    void checkForEmptyMovieListWithNameParam() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/movies/Scream"))
                .andExpect(status().isOk())
                .andExpect(content().json("[]"));
    }

    //Testfall 2
    @Test
    void checkForAddedMovieWithNameParam() throws Exception {
        String movieName = "Prey";
        Movie movie = new Movie(movieName);

        mockMvc.perform(MockMvcRequestBuilders
                .post("/movies")
                .content(objectMapper.writeValueAsString(movie))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON));

        mockMvc.perform(MockMvcRequestBuilders
                        .get("/movies/Prey")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[0].id").value(0))
                .andExpect(jsonPath("$.[0].name").value(movieName));

        Movie expectedMovie = new Movie(0L, movieName);
        List<Movie> movieEntity = movieRepository.findMovieByName(expectedMovie.getName());
        Assertions.assertThat(movieEntity).isEqualTo(List.of(expectedMovie));
    }

    //Testfall 3
    @Test
    void checkForMultipleAddedMovieWithNameParam() throws Exception {

        String movieName = "Prey";
        Movie movie = new Movie(movieName);

        mockMvc.perform(MockMvcRequestBuilders
                .post("/movies")
                .content(objectMapper.writeValueAsString(movie))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON));

        mockMvc.perform(MockMvcRequestBuilders
                .post("/movies")
                .content(objectMapper.writeValueAsString(movie))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON));

        mockMvc.perform(MockMvcRequestBuilders
                        .get("/movies/Prey")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[0].id").value(0))
                .andExpect(jsonPath("$.[0].name").value(movieName))
                .andExpect(jsonPath("$.[1].id").value(1))
                .andExpect(jsonPath("$.[1].name").value(movieName))
                .andExpect(jsonPath("$.*", hasSize(2)));

        Movie expectedMovie1 = new Movie(0L, movieName);
        Movie expectedMovie2 = new Movie(1L, movieName);

        List<Movie> expectedMovies = List.of(expectedMovie1, expectedMovie2);
        List<Movie> allMovies = movieRepository.findAll();
        Assertions.assertThat(allMovies.size()).isEqualTo(2);
        List<Movie> movieEntity = movieRepository.findMovieByName(expectedMovie1.getName());
        Assertions.assertThat(movieEntity).isEqualTo(expectedMovies);
    }

}