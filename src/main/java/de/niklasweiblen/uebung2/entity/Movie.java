package de.niklasweiblen.uebung2.entity;

import lombok.*;

import javax.persistence.*;

@EqualsAndHashCode
@Entity
@Table
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
public class Movie {

    @Getter
    @Setter
    @Id @GeneratedValue(strategy = GenerationType.TABLE, generator = "movieGeneration")
    @TableGenerator(name = "movieGeneration", initialValue = -1)
    private Long id;

    @Getter
    @Setter
    @NonNull
    private String name;
}
