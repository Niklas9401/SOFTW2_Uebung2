package de.niklasweiblen.uebung2.service;

import de.niklasweiblen.uebung2.entity.Movie;

import java.util.List;
import java.util.Optional;

public interface MovieService {

    void addMovie(Movie movie);
    Optional<Movie> getMovieWithId(Long id);
    List<Movie> getMovieWithName(String name);
    List<Movie> getAllMovies();
}
