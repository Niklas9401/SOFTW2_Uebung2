package de.niklasweiblen.uebung2.service;

import de.niklasweiblen.uebung2.entity.Movie;
import de.niklasweiblen.uebung2.repository.MovieRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class MovieServiceImplementation implements MovieService {

    private final MovieRepository movieRepository;

    @Override
    public void addMovie(Movie movie) {
        log.info("Adding movie {} to database", movie.getName());
        movieRepository.save(movie);
    }

    @Override
    public Optional<Movie> getMovieWithId(Long id) {
        return movieRepository.findMovieById(id);
    }

    @Override
    public List<Movie> getMovieWithName(String name) {
        return movieRepository.findMovieByName(name);
    }

    @Override
    public List<Movie> getAllMovies() {
        return movieRepository.findAll();
    }
}
