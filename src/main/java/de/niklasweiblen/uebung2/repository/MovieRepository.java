package de.niklasweiblen.uebung2.repository;

import de.niklasweiblen.uebung2.entity.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Long> {


    Optional<Movie> findMovieById(Long id);
    List<Movie> findMovieByName(String name);
}
