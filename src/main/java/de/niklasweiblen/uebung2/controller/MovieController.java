package de.niklasweiblen.uebung2.controller;

import de.niklasweiblen.uebung2.entity.Movie;
import de.niklasweiblen.uebung2.service.MovieService;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Controller
@Transactional
@AllArgsConstructor
@Slf4j
@RequestMapping("/movies")
public class MovieController {

    @Autowired
    private MovieService movieService;

    @GetMapping
    public ResponseEntity<List<Movie>> getAllMovies() {
        log.info("Fetching all movies from the movielist");
        return ResponseEntity.ok().body(movieService.getAllMovies());
    }

    @PostMapping
    public ResponseEntity<Movie> addMovie(@RequestBody Movie movieToBeAdded) {
        if (movieToBeAdded.getName() == null || movieToBeAdded.getName().isEmpty()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        movieService.addMovie(movieToBeAdded);
        return new ResponseEntity<>(HttpStatus.CREATED);

    }

    @GetMapping(value = "/{id:\\d+}")
    public ResponseEntity<Movie> getMovieById(@PathVariable(required = true) Long id) {
        log.info("Searching movie with id {}", id);
        Optional<Movie> movieSearchResult = movieService.getMovieWithId(id);
        if (movieSearchResult.isEmpty())
            return ResponseEntity.notFound().build();
        return ResponseEntity.ok().body(movieSearchResult.get());
    }

    @GetMapping(value = "/{name:\\D+}")
    public ResponseEntity<List<Movie>> getMoviesByName(@PathVariable(required = true) String name) {
        log.info("Searching movie with name {}", name);
        List<Movie> movieSearchResult = movieService.getMovieWithName(name);
        return ResponseEntity.ok().body(movieSearchResult);
    }

}
