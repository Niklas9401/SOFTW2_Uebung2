# Softwaretechnik - modulbezogene Übung Teil 2

Entwickeln einer API im Spring-Framework und anschließendes Ausführen von Tests (Coverage, Unit, Integration und Mutationstest).

## Implementierte Features in Spring

- Erstellen von Filmen
- Auslesen von Filmen sowie der dazugehörigen ID
- Auslesen von Filmen anhand einer ID
- Auslesen von Filmen anhand des Filmnamens

Datensätze sind wie gewollt in einem Docker-Container mit Postgres gespeichert.
Die Datensätze bleiben bei Neustart der Anwendung erhalten. Zum "Neu erstellen" und damit auch dem Löschen der Datensätze wird ein Batch-Skript mitgeliefert (recreate_docker_db.bat)

## API Endpunkte
#### Get all movies

```http
  GET /movies/
```

Returns list of all movies.

<br />

#### Add movies

```http
  POST /movies/
```

| Parameter   | Type     | Description                 |
|:------------|:---------|:----------------------------|
| `movieName` | `String` | **Required**. Name of movie |


Returns status code 201 CREATED and adds the given movie to the movieList.

<br />

#### Get movie by id

```http
  GET /movies/{id}
```

| Parameter | Type     | Description               |
|:----------|:---------|:--------------------------|
| `id`      | `String` | **Required**. ID of movie |


Returns, when movie with `id` exists: Status code 200 OK and the specified movie of the movieList.

Returns, when movie with `id` NOT exists: Status code 404 NOT FOUND.

<br>

#### Get movie by name

```http
  GET /movies/{name}
```

| Parameter  | Type     | Description                 |
|:-----------|:---------|:----------------------------|
| `name`     | `String` | **Required**. Name of movie |


Returns, when movie with `name` exists: Status code 200 OK and the specified movie in a list.

Returns, when movie with `id` NOT exists: Status code 200 OK with an empty list.

## Authors

- [@Niklas9401](https://github.com/Niklas9401)